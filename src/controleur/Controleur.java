package controleur;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

import modele.Evenement;
import modele.FriseChronologique;
import modele.LectureEcriture;
import vue.PanelAffichage;
import vue.PanelCreation;


/**
 * Classe Controleur qui synchronise nos 2 conteneurs principaux
 * @see PanelCreation
 * @see PanelAffichage
 * Implemente l'interface ActionListener
 * @see ActionListener
 * @author Mehdi
 *
 */
public class Controleur implements ActionListener{
	/**
	 * Frise chronologique � synchroniser
	 * @see FriseChronologique
	 */
	FriseChronologique frise;
	
	/**
	 * PanelCreation � synchroniser
	 * @see PanelCreation
	 */
	PanelCreation panelCreation;
	
	/**
	 * PanelAffichage � synchroniser
	 * @see PanelAffichage
	 */
	PanelAffichage panelAffichage;
	
	
	/**
	 * Constructeur de la classe Controleur
	 * Le constructeur met � l'�coute les 2 panels
	 * @param parFrise FriseChronologique � synchro
	 * @param parPanelCreation PanelCreation � Synchro
	 * @param parPanelAffichage PanelAffichage � Synchro
	 */
	public Controleur(FriseChronologique parFrise,PanelCreation parPanelCreation, PanelAffichage parPanelAffichage) {		
		frise = parFrise;
		panelCreation = parPanelCreation;
		panelAffichage = parPanelAffichage;
		panelCreation.enregistreEcouteur(this);
		panelAffichage.enregistreEcouteur(this);	
	}

	
	/**
	 * Methode actionPerformed
	 * @param parEvt ActionEvt
	 * Traite 5 cas :
	 * Celui ou l'utilisateur clique sur Cr�er une nouvelle frise : r�cup�re les param�tres saisies
	 * Celui ou l'utilisateur ajoute un �v�nement a une frise : r�cup�re les param�tres saisies
	 * Celui ou l'utilisateur clique sur charger une Frise : lis le fichier � l'adresse saisie
	 * Celui ou l'utilisateur clique sur le bouton suivant du diaporama : Avance la scrollBar en meme temps que changer l'�v�nement suivant
	 * Celui ou l'utilisateur clique sur le bouton pr�c�dent du diaporama : Recule la scrollBar en meme temps que changer l'�v�nement pr�c�dent
	 * Celui ou l'utilisateur veut charger une image : r�cupere l'adresse de l'image
	 * Et enfin celui ou l'utilisateur veut choisir ou sauvegarder sa frise : recupere l'adresse du repertoire
	 */
	public void actionPerformed(ActionEvent parEvt) {
		
		//Cas ou l'on creer une frise
		if(parEvt.getActionCommand().equals("FRISE")) {
			frise = panelCreation.getPanelFrise().getFrise();
			panelCreation.getPanelFrise().reset();
			panelAffichage.getPanelFrise().setFrise(frise);
			panelAffichage.getDiapo().setFrise(frise);
			panelCreation.getFormulaire().setFrise(frise);
			File fichier = new File(frise.getAdresse()+File.separator+frise.getIntitule()+".ser");
			LectureEcriture.ecriture(fichier, frise);
		}
		
		//Cas ou l'ont ajoute un evenement a une frise
		else if(parEvt.getActionCommand().equals("FORMULAIRE")) {
			Evenement evt = panelCreation.getFormulaire().getEvenement();
			frise.ajout(evt);	
			panelAffichage.getPanelFrise().setFrise(frise);
			panelAffichage.getDiapo().setFrise(frise);
			panelCreation.getFormulaire().reset();
			File fichier = new File(frise.getAdresse()+File.separator+frise.getIntitule()+".ser");
			LectureEcriture.ecriture(fichier, frise);
		}
		
		//Cas ou on charge une frise
		else if(parEvt.getActionCommand().equals("SELECTION")) {
				JFileChooser dialogue = new JFileChooser();		        
		        // affichage
		        dialogue.showOpenDialog(null);
		        if (dialogue.getSelectedFile() != null)
		        	frise = (FriseChronologique) LectureEcriture.lecture(dialogue.getSelectedFile());
				panelAffichage.getPanelFrise().setFrise(frise);
				panelCreation.getFormulaire().setFrise(frise);
				panelAffichage.getDiapo().setFrise(frise);
				panelCreation.getPanelFrise().reset();
		}
		
		//Diapo suivant
		else if(parEvt.getActionCommand().equals("BoutonDroit")) {
			panelAffichage.getDiapo().evenementSuivant();
			int annee = panelAffichage.getDiapo().getAnnneAfficher();
			panelAffichage.getPanelFrise().setScroll(annee);
		}
		
		//Diapo pr�c�dent
		else if (parEvt.getActionCommand().equals("BoutonGauche")) {
			panelAffichage.getDiapo().evenementPrecedent();
			int annee = panelAffichage.getDiapo().getAnnneAfficher();
			panelAffichage.getPanelFrise().setScroll(annee);
		}
		
		//Chargement d'une image
		else if (parEvt.getActionCommand().equals("IMAGE")) {
			JFileChooser dialogue = new JFileChooser();	        
	        // affichage
			String adresse;
	        dialogue.showOpenDialog(null);	        
	        
	        if (dialogue.getSelectedFile() != null) {
	        	File fichier = dialogue.getSelectedFile();
	        	adresse = fichier.getAbsolutePath();
	        	panelCreation.getFormulaire().setAdresseImage(adresse);
	        }
		}
		
		//Sauvegarde
		else if (parEvt.getActionCommand().equals("SAUVEGARDE")) {
			JFileChooser dialogue = new JFileChooser();	 
			dialogue.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        // affichage
			String adresse;
	        dialogue.showOpenDialog(null);   
	        if (dialogue.getSelectedFile() != null) {
	        	File fichier = dialogue.getSelectedFile();
	        	adresse = fichier.getAbsolutePath();
	        	panelCreation.getPanelFrise().setAdresseSAV(adresse);
	        }
	        
		}
		
	}	 

}
