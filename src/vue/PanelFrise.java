package vue;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import modele.Evenement;
import modele.FriseChronologique;
import modele.ModeleTable;

/**
 * Classe PanelFrise qui correspond � une frise (JTable)
 * Cette classe h�rite de JPanel, elle se sers de la classe ModeleTable
 * @see JPanel
 * @see ModeleTable
 * @author Mehdi
 *
 */
@SuppressWarnings("serial")
public class PanelFrise extends JPanel {
	
	/**
	 * Le modele de la table
	 */
	ModeleTable modele;
	
	/**
	 * La table elle m�me
	 */
	JTable tableFrise;
	
	/**
	 * La frise que l'on va associ� � la table
	 */
	FriseChronologique frise;
	
	/**
	 * Le scrollpane auquel on ajoute la table
	 */
	JScrollPane scrollPane;
	
	/**
	 * Le diaporama afin de le synchronis�
	 */
	PanelDiaporama panelDiaporama;
	
	
	/**
	 * Constructeur de la classe, il re�oit une frise et un diapo
	 * On instancie chaque �l�ments et on effectue les r�glages comme la taille etc..
	 * On affecte un renderer � la table
	 * On traite les �v�nements de la souris lorsque l'utilisateur clique sur une case
	 * de la table
	 * @param parFrise FriseChronologique
	 * @param parPanelDiaporama PanelDiaporama
	 */
	public PanelFrise(FriseChronologique parFrise,PanelDiaporama parPanelDiaporama) {
		frise = parFrise;
	 	modele = new ModeleTable(frise);
		tableFrise = new JTable(modele);
		tableFrise.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.setBackground(new Color(100,100,100));
		tableFrise.getTableHeader().setResizingAllowed(false);
		tableFrise.getTableHeader().setReorderingAllowed(false);
		tableFrise.setRowHeight(50);
		tableFrise.setDefaultRenderer(Evenement.class, new CelluleRenderer());
		scrollPane = new JScrollPane(tableFrise,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panelDiaporama = parPanelDiaporama;
		tableFrise.addMouseListener(new MouseAdapter(){ 
			public void mouseClicked(MouseEvent evt) {
				JTable table = (JTable) evt.getSource();
				ModeleTable model = (ModeleTable) table.getModel();
				Point point = evt.getPoint();
				int rowIndex = table.rowAtPoint(point);
				int colIndex = table.columnAtPoint(point);				
				if (model.getValueAt(rowIndex, colIndex) != null) 
					panelDiaporama.gestionnaireDeCartes.show(panelDiaporama.getPanelCentre(), ((Evenement) model.getValueAt(rowIndex, colIndex)).getTitre());				
			}			
		});
		scrollPane.setPreferredSize(new Dimension(1000,300));
		this.add(scrollPane);
	}
	
	/**
	 * M�thode qui modifie le mod�le de la table
	 * @param parFrise FriseChronologique
	 */
	public void setFrise(FriseChronologique parFrise) {
		frise = parFrise;
		tableFrise.setModel(new ModeleTable(frise));
	} 
	
	/**
	 * Accesseur de la frise
	 * @return FriseChronologique
	 */
	public FriseChronologique getFrise() {
		return this.frise;
	}
	
	/**
	 * Accesseur du JScrollPane
	 * @return JScrollPane
	 */
	public JScrollPane getScrollPane() {
		return this.scrollPane;
	}
	
	/**
	 * Methode qui modifie la position de la ScrollBar
	 * On determine le pourcentage que repr�sente l'ann�e par rapport
	 * � la frise et on fait bouger la barre de d�filement en fonction
	 * @param parAnnee int
	 */
	public void setScroll(int parAnnee) {
		double pourcentage = (double) (parAnnee - frise.getDateDebut()) / (frise.getDateFin() - frise.getDateDebut());
        double resultat = 1500 * pourcentage;
        JScrollBar scrollBar = scrollPane.getHorizontalScrollBar();
        scrollBar.setValue(0);
        scrollBar.setValue(scrollBar.getValue() + (int) resultat);
	}

}
