package vue;


import javax.swing.JPanel;

import controleur.Controleur;
import modele.ConstantesCouleursFontes;

/**
 * Cette classe h�rite de JPanel, ce panel
 * correspond � la 1�re partie de notre application
 * il contient le formulaire d'ajout d'�v�nements et celui pour
 * cr�er/charger une frise
 * @see JPanel
 * @author Driss
 *
 */
@SuppressWarnings("serial")
public class PanelCreation extends JPanel{
	
	/**
	 * Panel pour cr�er/charger une frise
	 */
	private PanelNouvelleFrise panelNouvelleFrise;
	
	/**
	 * Panel pour ajouter un �v�nement � une frise
	 */
	private PanelFormulaire panelFormulaire;
	
	/**
	 * Constructeur de la classe qui instance les champs et
	 * les ajoute au panel
	 */
	public PanelCreation() {
		
		panelNouvelleFrise = new PanelNouvelleFrise();
		panelFormulaire = new PanelFormulaire();
		setBackground(ConstantesCouleursFontes.CREAM);
		this.add(panelNouvelleFrise);
		this.add(panelFormulaire);
	}
	
	/**
	 * Accesseur de notre PanelNouvelleFrise
	 * @return PanelNouvelleFrise
	 */
	public PanelNouvelleFrise getPanelFrise() {
		return this.panelNouvelleFrise;
	}
	
	/**
	 * Accesseur de notre PanelFormulaire
	 * @return PanelFormulaire
	 */
	public PanelFormulaire getFormulaire() {
		return this.panelFormulaire;
	}
	
	/**
	 * Methode qui met le controleur � l'�coute de nos 2 panels
	 * pour la synchronisation...
	 * @param controleur Controleur
	 */
	public void enregistreEcouteur(Controleur controleur) {
		panelNouvelleFrise.enregistreEcouteur(controleur);
		panelFormulaire.enregistreEcouteur(controleur);
	}

}
