package vue;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controleur.Controleur;
import modele.FriseChronologique;
import modele.LectureEcriture;

/**
 * Conteneur (ContentPane) de notre application.
 * Cette classe impl�mente ActionListene afin de pouvoir
 * cliquer les items de menus
 * Elle instancie �galement nos 2 prinicpaux panels et le controleur
 * pour la synchronisation.
 * @author Driss
 *
 */
@SuppressWarnings("serial")
public class PanelFiche extends JPanel implements ActionListener {
	
	/**
	 * Ensemble de nos �l�ments graphiques
	 * + controleur
	 * + CardLayout pour empiler les �l�ments
	 */
	private FriseChronologique frise;
	private PanelCreation panelCreation;
	private PanelAffichage panelAffichage;
	private Controleur controleur;
	private CardLayout gestionnaireDeCartes;
	
	
	/**
	 * Constructeur de la classe.
	 * On instancie chaque �l�ment avec leurs constructeurs et
	 * param�tres associ�s
	 * Ce panel est g�r� par un CardLayout
	 */
	public PanelFiche () {
	
		//On charge une  frise d�ja cr�er pour l'exemple
		//frise = (FriseChronologique) LectureEcriture.lecture(new File("C:\\Users\\Mehdi\\Desktop\\IUT_travail\\workspace\\ProjetTutor�\\frises\\Classe C Mercedes.ser"));
		frise = (FriseChronologique) LectureEcriture.lecture(new File(".\\frises\\Classe C Mercedes.ser"));
		
		
		gestionnaireDeCartes = new CardLayout(50,50);
		this.setLayout(gestionnaireDeCartes);
		panelCreation = new PanelCreation();
		this.add(panelCreation,"Cr�ation");
		
		panelAffichage = new PanelAffichage(frise);
		this.add(panelAffichage,"Affichage");		
		panelCreation.getFormulaire().setFrise(frise);		
		controleur = new Controleur(frise,panelCreation,panelAffichage);
		
		
	}
	
	
	/**
	 * Accesseur du PanelCreation
	 * @return PanelCreation
	 */
	public PanelCreation getPanelCreation() {
		return this.panelCreation;
	}
	
	/**
	 * Accesseur du controleur
	 * @return Controleur
	 */
	public Controleur getControleur() {
		return this.controleur;
	}

	
	/**
	 * Methode actionPerformed qui traite le cas ou l'utilisateur
	 * clique sur un item de menu, selon l'item cliqu�, cette m�thode
	 * affiche la carte correspondante ( cr�ation, affichage, quitter ou informations )
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand().equals("Cr�ation")) {
			gestionnaireDeCartes.show(this, event.getActionCommand());
		}
		
		else if (event.getActionCommand().equals("Affichage")) {
			gestionnaireDeCartes.show(this, event.getActionCommand());
		}
		else if (event.getActionCommand().equals("Quitter")) {
			int reponse = JOptionPane.showConfirmDialog(this,"Etes-vous sur de vouloir fermer ?"
					,"Fermeture de la fen�tre"
					,JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);			
			if(reponse == JOptionPane.YES_OPTION)
				System.exit(0);
		}
		
		else if (event.getActionCommand().equals("?")) {			
			JOptionPane.showMessageDialog(this, (Object) "ATTENTION ! Veillez � remplir tous les champs  qui demandent un nombre sinon erreur ! "
					+ "Application d�velopp�e par LEQSIOUER Mehdi & NAIT BELKACEM Driss =)", "Mini manuel d'utilisation", 1);
		}		
		
	}

}
