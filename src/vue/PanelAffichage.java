package vue;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import controleur.Controleur;
import modele.FriseChronologique;

/**
 * Conteneur de l'affichage, cette classe correspond
 *  a la deuxi�me partie de l'application, on y trouve
 *  la frise et le diaporama
 *  ELle h�rite de JPanel
 *  @see JPanel
 * @author Mehdi
 *
 */
@SuppressWarnings("serial")
public class PanelAffichage extends JPanel {
	
	/**
	 * Notre panel qui contient la JTable
	 */
	private PanelFrise panelFrise;
	
	/**
	 * Notre diaporama qui correspond aux �v�nements de la frise
	 */
	private PanelDiaporama panelDiaporama;
	
	/**
	 * Frise Chronologique associ�e
	 */
	private FriseChronologique frise;
	
	
	/**
	 * Constructeur de la classe qui recoit une frise
	 * on instancie nos 2 panels en envoyant les bons param�tres
	 * le panel est g�r� par un BorderLayout
	 * @param parFrise FriseChronologique
	 */
	public PanelAffichage(FriseChronologique parFrise) {		
		setLayout(new BorderLayout());	
		frise = parFrise;
		
		panelDiaporama = new PanelDiaporama(frise);
		panelFrise = new PanelFrise(frise,panelDiaporama);
		
		this.add(panelFrise, BorderLayout.SOUTH);
		this.add(panelDiaporama, BorderLayout.NORTH);
	}

	/**
	 * Methode qui met le Controleur � l'�coute du PanelDiaporama
	 * Pour la synchronisation...
	 * @param controleur Controleur
	 */
	public void enregistreEcouteur(Controleur controleur) {
		panelDiaporama.enregistreEcouteur(controleur);
		
	}
	
	/**
	 * Accesseur du PanelFrise
	 * @return PanelFrise
	 */
	public PanelFrise getPanelFrise() {
		return this.panelFrise;
	}
	
	/**
	 * Accesseur du PanelDiaporama
	 * @return PanelDiaporama
	 */
	public PanelDiaporama getDiapo() {
		return this.panelDiaporama;
	}
	
	/**
	 * Mutateur qui modifie la frise de notre panel
	 * @param parFrise FriseChronologique
	 */
	public void setFrise(FriseChronologique parFrise) {
		this.frise = parFrise;
	}

}
