package vue;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JComboBox;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controleur.Controleur;
import modele.Data;
import modele.Date;
import modele.Evenement;
import modele.FriseChronologique;


/**
 * Classe PanelFormulaire qui h�rite de JPanel,
 * Cette classe correspond � un formulaire d'ajout d'�v�nements
 * l'utilisateur doit saisir des donn�es dans des champs et cliquer
 * sur un bouton pour ajouter l'�v�nement
 * @see JPanel
 * @author Driss
 *
 */
@SuppressWarnings("serial")
public class PanelFormulaire extends JPanel{
	
	/**
	 * Ensemble de nos �l�ments graphique : label,boutons,combobox,textfield...
	 * Les constantes sont dans la classe Data
	 * @see Data
	 */
	public String INTITULE_BOUTON_AJOUT = Data.INTITULE_BOUTON_AJOUT_FORMULAIRE;
	
	String [] minutes = new String[60];
	
	FriseChronologique friseActuel;
	//Datee
	JLabel labelCreation = new JLabel("Ajouts d'�v�nements");
	JButton boutonSuivant = new JButton(INTITULE_BOUTON_AJOUT);
	
	JLabel labelChoixFrise = new JLabel("Frise");
	JLabel friseActuelle = new JLabel();
	
	//Titre
	JLabel labelTitre = new JLabel("Titre");
	JTextField saisieTitre = new JTextField(20);
	
	//Lieu
	JLabel labelDate = new JLabel("Date : Jour / Mois / Ann�e ");
	JComboBox<String> saisieJour = new JComboBox<String>(Data.jour);
	
	JComboBox<String> saisieMois = new JComboBox<String>(Data.mois);
	
	JTextField saisieAnnee = new JTextField(5);
	
	JLabel labelAdresse = new JLabel("Image");
	//JTextField saisieAdresse = new JTextField(20);
	JButton saisieAdresse = new JButton("Charger l'image");
	
	JLabel labelTexte = new JLabel("Texte descriptif");
	JTextArea saisieTexte = new JTextArea(10,20);
	
	JLabel labelPoids = new JLabel("Poids (importance) :");
	//JTextField saisiePoids = new JTextField(5);
	JComboBox<String> saisiePoids = new JComboBox<String>(Data.poids);
	
	String adresseimage;
	
	/**
	 * Constructeur de la classe
	 * On place les mn�moniques pour l'ergonomie de l'application
	 * ce panel est g�r� par un GridBagLayout pour pouvoir placer
	 * les �l�ments comme on veut.
	 * On ajoute les �l�ments un par un au panel selon les contraintes choisies
	 * On met le controleur � l'�coute du bouton d'ajout d'�v�nement 
	 */
	public PanelFormulaire() {		
		setLayout(new GridBagLayout());
		this.setBackground(new Color(225, 206, 154));
		
		
		
		
		//mise en place des Mnemoniques
		labelTitre.setLabelFor(saisieTitre);
		labelDate.setLabelFor(saisieJour);
		labelAdresse.setLabelFor(saisieAdresse);
		labelTexte.setLabelFor(saisieTexte);
		labelPoids.setLabelFor(saisiePoids);
		
		labelChoixFrise.setDisplayedMnemonic('F');
		labelTitre.setDisplayedMnemonic('T');
		labelDate.setDisplayedMnemonic('D');
		labelAdresse.setDisplayedMnemonic('A');
		labelTexte.setDisplayedMnemonic('T');
		labelPoids.setDisplayedMnemonic('P');
				
		//Couleurs bouton
		boutonSuivant.setBackground(Color.LIGHT_GRAY);
		saisieAdresse.setBackground(Color.LIGHT_GRAY);
		
		GridBagConstraints contrainte = new GridBagConstraints();		
		contrainte.insets = new Insets(10,10,10,10);
		contrainte.anchor = GridBagConstraints.NORTHWEST;
		
		
		//Date
		contrainte.gridx = 0; contrainte.gridy = 0;		
		this.add(labelCreation,contrainte);
		
		contrainte.gridwidth = 4;
		contrainte.anchor = GridBagConstraints.EAST;
		contrainte.gridx = 1;
		boutonSuivant.setActionCommand("FORMULAIRE");
		this.add(boutonSuivant,contrainte);
		
		
		contrainte.anchor = GridBagConstraints.NORTHWEST;
		
		//choixFrise
		contrainte.gridx = 0;
		contrainte.gridy = 1;
		this.add(labelChoixFrise,contrainte);
		
		contrainte.gridx = 1;
		contrainte.gridwidth = 3;
		this.add(friseActuelle,contrainte);
		
		//Titre
		contrainte.gridwidth = 1;
		contrainte.gridx = 0;		
		contrainte.gridy = 2;		
		this.add(labelTitre,contrainte);
		
		contrainte.gridx = 1;
		contrainte.gridwidth = 3;
		this.add(saisieTitre,contrainte);
		
		//date
		contrainte.gridwidth = 1;
		contrainte.gridx = 0;
		contrainte.gridy = 3;		
		this.add(labelDate,contrainte);
		
		contrainte.gridx = 1;		
		this.add(saisieJour,contrainte);
		
		contrainte.gridwidth = 2;
		contrainte.gridx = 2;		
		this.add(saisieMois,contrainte);
		
		contrainte.gridwidth = 2;
		contrainte.gridx = 3;		
		this.add(saisieAnnee,contrainte);
		
		//adresse
		contrainte.gridx = 0;
		contrainte.gridy = 4;		
		this.add(labelAdresse,contrainte);
		
		
		contrainte.gridx = 1;
		saisieAdresse.setActionCommand("IMAGE");		
		this.add(saisieAdresse,contrainte);
		
		//poids
		contrainte.gridx = 0;
		contrainte.gridy = 5;
		this.add(labelPoids,contrainte);
		
		contrainte.gridx = 1;
		this.add(saisiePoids,contrainte);
			
		
		//Fin
		contrainte.gridx = 0;
		contrainte.gridy = 6;
		this.add(labelTexte,contrainte);
		
		contrainte.gridx = 1;
		contrainte.gridwidth = 3;
		this.add(saisieTexte,contrainte);		

		reset();
		
	}//PanelFormulaire()	

	
	/**
	 * M�thode qui "reset" le panel en entier,il remet les champs
	 * � vide comme au d�part
	 */
	public void reset() {
		//On r�initialise les champs et on met le focus sur la premiere zone
		saisieTitre.setText("");
		saisieJour.setSelectedIndex(0);
		saisieMois.setSelectedIndex(0);
		saisieAnnee.setText("");
		//saisieAdresse.setText("");
		saisieTexte.setText("");
		saisiePoids.setSelectedIndex(0);
		saisieTitre.requestFocus();		
	}

	
	/**
	 * Methode qui met le controleur � l'�coute du bouton
	 * @param controleur Controleur
	 */
	public void enregistreEcouteur(Controleur controleur) {
		boutonSuivant.addActionListener(controleur);
		saisieAdresse.addActionListener(controleur);
		
	}

	/**
	 * Accesseur de l'intitul� du bouton
	 * @return String
	 */
	public String getIntituleBoutonAjout() {
		return INTITULE_BOUTON_AJOUT;
	}

	
	/**
	 * Methode qui construit un �v�nement � partir des donn�es saisies dans chacun
	 * des champs du formulaire
	 * @return Evenement
	 * @see Evenement
	 */
	public Evenement getEvenement() {
		return new Evenement(new Date(Integer.parseInt((String) saisieJour.getSelectedItem()),Integer.parseInt((String) saisieMois.getSelectedItem()),
				Integer.parseInt(saisieAnnee.getText())),saisieTitre.getText(),adresseimage,
				saisieTexte.getText(),Integer.parseInt((String) saisiePoids.getSelectedItem()),(FriseChronologique) friseActuel);
	}
	
	
	/**
	 * Methode qui renvoie le champ qui doit recevoir le focus
	 * ici c'est le 1er donc saisieTitre
	 * @return JTextField
	 */
	public JTextField getChampRecevantFocus() {
		return saisieTitre;
	}
	
	/**
	 * M�thode qui modifie la frise � laquelle seront ajout� les
	 * �v�nements rentr� dans le formulaire
	 * @param parFrise FriseChronologique
	 * @see FriseChronologique
	 */
	public void setFrise(FriseChronologique parFrise) {
		this.friseActuel = parFrise;
		friseActuelle.setText(friseActuel.getIntitule());
	}
	
	/**
	 * Methode qui modifie l'adresse de l'image selon celle charger
	 * @param parAdresse String
	 */
	public void setAdresseImage(String parAdresse) {
		adresseimage = parAdresse;
	}
}
