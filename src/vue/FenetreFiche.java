package vue;

import java.awt.Color;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import modele.Data;


/**
 * Fenetre m�re de notre application, elle h�rite de JFrame
 * et contient le main
 * @see JFrame
 * @author Driss
 *
 */
@SuppressWarnings("serial")
public class FenetreFiche extends JFrame {
	
	/**
	 * Constructeur de la classe, il recoit un titre
	 * et on appel le constructeur m�re en lui envoyant le titre,
	 * qui sera le nom de la fen�tre
	 * On instancie notre Conteneur qui est un PanelFiche
	 * et on ajoute un menu a la fen�tre, et on met notre conteneur
	 * � l'�coute des items du menus.
	 * @param parTitre String
	 */
	public FenetreFiche(String parTitre) {
		super(parTitre);
		PanelFiche contentPane = new PanelFiche();
		
		
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
	
		final int NB_ITEM = 4;
		JMenuItem [] itemMenu = new JMenuItem[NB_ITEM];
		
		for(int i = 0; i < NB_ITEM; i ++ ) {
			itemMenu[i] = new JMenuItem(Data.INTITULE_ITEM[i],Data.INTITULE_ITEM[i].charAt(0));
			itemMenu[i].addActionListener(contentPane);
			itemMenu[i].setActionCommand(Data.INTITULE_ITEM[i]);
			itemMenu[i].setAccelerator(KeyStroke.getKeyStroke(Data.INTITULE_ITEM[i].charAt(0),java.awt.Event.CTRL_MASK));
			menuBar.add(itemMenu[i]);
		}
		itemMenu[3].setAccelerator(KeyStroke.getKeyStroke('I',java.awt.Event.CTRL_MASK));
		setContentPane(contentPane);
		contentPane.setBackground(Color.DARK_GRAY);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(1800,900);
		setVisible(true);
		setResizable(false);
		//Focus sur le premier champ de saisi
		contentPane.getPanelCreation().getFormulaire().getChampRecevantFocus().requestFocus();
		setLocation(100,150);
		setBackground(Color.DARK_GRAY);
 	}//Constructeur
	
	
	/**
	 * Main, c'est la ou le programme d�marre
	 * @param args String[]
	 */
	public static void main (String [] args) {		
		new FenetreFiche("Projet Tutor�");
	}//Main
	
	/**
	 * Methode insets qui correspond aux bordure
	 * @see Insets
	 */
	public Insets getInsets() {
		return new Insets(40,15,15,15);
	}//Method Insets
	
	
}//Class FenetreMere
