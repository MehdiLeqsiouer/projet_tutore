package vue;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;

import controleur.Controleur;
import modele.Data;
import modele.Evenement;
import modele.FriseChronologique;

/**
 * Classe qui correspond au diaporama des �v�nements de la frise
 * Elle h�rite de JPanel et impl�mente l'interface ActionListener
 * @see JPanel
 * @see ActionListener
 * @author Mehdi
 *
 */
@SuppressWarnings("serial")
public class PanelDiaporama extends JPanel {
	
	/**
	 * Notre CardLayout (diapo)
	 */
	CardLayout gestionnaireDeCartes;
	
	/**
	 * Frise associ�e au diapo
	 */
	FriseChronologique frise;
	
	/**
	 * Ensemble des �l�mens de notre Diaporama : boutons, label...
	 */
	private JPanel panelCentre;
	JLabel label;
	JButton boutonGauche = new JButton(Data.INTITULE_BOUTON_GAUCHE);
	JButton boutonDroit = new JButton(Data.INTITULE_BOUTON_DROITE);
	GridBagConstraints contrainte = new GridBagConstraints();
	JLabel labelTitre;
	
	/**
	 * Constructeur de la classe, ce panel est g�r� par un GridBagLayout afin
	 * de mieux positionner les �l�ments
	 * On fait ajoute le titre de la frise en haut
	 * Les �l�ments de la la frise empil�s dans un panel au centre (qui lui est g�r� par un CardLayout)
	 * et des boutons de part et d'autre pour naviguer dans le diaporama
	 * @param parFrise FriseChronologique
	 */
	public PanelDiaporama(FriseChronologique parFrise) {
		super();		
		
		this.setLayout(new GridBagLayout());
		
		
		gestionnaireDeCartes = new CardLayout();
		frise = parFrise;
		setEvenement(frise,gestionnaireDeCartes);		
	
		boutonGauche.setActionCommand("BoutonGauche");
		boutonDroit.setActionCommand("BoutonDroit");
		boutonGauche.setBackground(Color.lightGray);
		boutonDroit.setBackground(Color.LIGHT_GRAY);				
		
	}
	
	/**
	 * Methode qui instancie le panel au centre de notre PanelDiaporama, ce panel au centre
	 * contient tous les �v�nements de la Frise sous format : image, date, titre et texte
	 * On r�cup�re simplement tous les �v�nements de la frise et on les ajoutes
	 * Le tout g�rer par un CardLayout
	 * On ajoute �galement les 2 boutons
	 * @param parFrise FriseChronologique
	 * @param parGestionnaire CardLayout
	 */
	public void setEvenement(FriseChronologique parFrise,CardLayout parGestionnaire) {
		this.removeAll();
		panelCentre = new JPanel();
		frise = parFrise;		
		gestionnaireDeCartes = parGestionnaire;
		panelCentre.setLayout(gestionnaireDeCartes);
		int annee = frise.getDateDebut();
		int taille = frise.getDateFin()-frise.getDateDebut();
		for(int i = 0; i < taille; i ++) {
			Collection <Evenement> evts = frise.getEvenementsAnnee(annee);
			if(evts != null) {
				for(Evenement events : evts) {
					if (events != null ) {
						JPanel panelEvt = new JPanel();						
						label = new JLabel(events.toString());
						ImageIcon image = new ImageIcon(new ImageIcon(events.getAdresse()).getImage().getScaledInstance(500, 300, Image.SCALE_DEFAULT));
						panelEvt.add(new JLabel(image));
						panelEvt.add(label);
						panelEvt.setName(Integer.toString(events.getDate().getAnnee()));
						panelCentre.add(panelEvt,events.getTitre());
					}
				}
			}
			annee += 1;
		}
		contrainte.gridx = 1;
		contrainte.gridy = 0;
		contrainte.gridwidth = 2;
		labelTitre = new JLabel("<html><h2>"+frise.getIntitule()+"</h2></html>");
		this.add(labelTitre,contrainte);
		gestionnaireDeCartes.first(panelCentre);
		contrainte.gridx = 2;
		contrainte.gridy = 2;
		this.add(panelCentre,contrainte);
		contrainte.gridwidth = 1;
		contrainte.gridx = 0;
		contrainte.gridy = 2;
		this.add(boutonGauche, contrainte);
		contrainte.gridx = 5;
		this.add(boutonDroit, contrainte);
	}
	
	/**
	 * Methode qui ajoute un Label au panelCentre (qui est le panelDiaporama)
	 * Cette methode est appel�e dans le Controleur lorsque l'utilisateur ajoute
	 * un �v�nement 
	 * @param labelEvt JLabel
	 * @param image ImageIcon
	 */
	public void ajout(JLabel labelEvt, ImageIcon image) {
		JPanel panelEvt = new JPanel();
		panelEvt.add(new JLabel(image));
		panelEvt.add(labelEvt);
		panelCentre.add(panelEvt,labelEvt.getText());
	}

	/**
	 * Methode qui met le  controleur � l'�coute des 2 boutons pr�c�dent et suivant
	 * pour synchroniser la ScrollBar de notre JTable
	 * @see JScrollBar
	 * @param controleur Controleur
	 */
	public void enregistreEcouteur(Controleur controleur) {
		boutonGauche.addActionListener(controleur);
		boutonDroit.addActionListener(controleur);		
	}
	
	/**
	 * Methode qui affiche la carte suivante
	 */
	public void evenementSuivant() {
		gestionnaireDeCartes.next(panelCentre);
	}
	
	/**
	 * Methode qui affiche la carte pr�c�dente
	 */
	public void evenementPrecedent() {
		gestionnaireDeCartes.previous(panelCentre);
	}
	/**
	 * Accesseur de notre panelCentre (le diaporama)
	 * @return JPanel
	 */
	public JPanel getPanelCentre() {
		return this.panelCentre;
	}
	
	/**
	 * Methode qui modifie la frise du diaporama, en �crasant les
	 * �v�nements par ceux de la nouvelle Frise
	 * @param parFrise FriseChronologique
	 */
	public void setFrise(FriseChronologique parFrise) {
		this.frise = parFrise;
		setEvenement(frise,new CardLayout(5,5));
		this.labelTitre.setText("<html><h2>"+frise.getIntitule()+"</h2></html>");
	}
	
	/**
	 * Methode qui renvoie l'ann�e du composant affich� sur le diaporama
	 * composant unique car les autres sont empil�s par le CardLayout
	 * @return int l'ann�e de l'�v�nement
	 */
	public int getAnnneAfficher() {
		JPanel panel = null;
		for (Component comp : panelCentre.getComponents()) {
			if (comp.isVisible()) {
				panel = (JPanel) comp;
			}
		}
		if (panel != null)
			return Integer.parseInt(panel.getName());
		return 0;
	}
	
}
