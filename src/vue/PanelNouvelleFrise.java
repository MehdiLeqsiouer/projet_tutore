package vue;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controleur.Controleur;
import modele.Data;
import modele.FriseChronologique;


/**
 * Classe PanelNouvelleFrise qui h�rite de JPanel
 * Cette classe permet � l'utilisateur de saisir des donn�es
 * pour cr�er ou charger une Frise
 * @see JPanel
 * @author Mehdi
 *
 */
@SuppressWarnings("serial")
public class PanelNouvelleFrise extends JPanel{
	/**
	 * Ensemble de nos �l�ments graphiques : boutons,label,textfield,combobox...
	 * Les constantes sont dans la classe Data
	 * @see Data
	 */
	
	public String INTITULE_BOUTON_AJOUT = Data.INTITULE_BOUTON_AJOUT_FRISE;	
	//Datee
	JLabel labelCreation = new JLabel("Cr�ation d'une frise");
	JButton boutonNewFrise = new JButton(INTITULE_BOUTON_AJOUT);
	
	
	//Titre
	JLabel labelTitre = new JLabel("Titre");
	JTextField saisieTitre = new JTextField(20);
	
	//Lieu
	JLabel labelPeriode = new JLabel("P�riode");
	JTextField saisiePeriode = new JTextField(5);
	
	
	//Debut
	JLabel labelDebut = new JLabel("Ann�e d�but");
	JTextField saisieDateDebut = new JTextField(5);
	
	
	//Fin
	JLabel labelFin = new JLabel("Ann�e fin");
	JTextField saisieDateFin = new JTextField(5);
	
	JLabel labelAdresse = new JLabel("Adresse de sauvegarde");
	JButton saisieAdresse = new JButton("Choisir ou sauvegarder");
	String adresseSAV = ".";
	
	JLabel separateur = new JLabel("------------------------------");
	
	JLabel labelChoixFrise = new JLabel("Choix d'une frise");
	JButton boutonChoixFrise = new JButton("Charger la frise");	

	
	/**
	 * Constructeur de la classe.
	 * Ce panel est g�r� par un GridBagLayout comme le formulaire
	 * On associe les mn�moniques
	 * On ajoute les �l�ments un par un en g�rant les contraintes
	 */
	public PanelNouvelleFrise() {		
		setLayout(new GridBagLayout());
		this.setBackground(new Color(225, 206, 154));	
		
		
		//mise en place des Mnemoniques
		labelTitre.setLabelFor(saisieTitre);
		labelPeriode.setLabelFor(saisiePeriode);
		labelDebut.setLabelFor(saisieDateDebut);
		labelFin.setLabelFor(saisieDateFin);
		labelAdresse.setLabelFor(saisieAdresse);
		//labelAdresseFrise.setLabelFor(saisieAdresseFrise);
		
		labelTitre.setDisplayedMnemonic('T');
		labelPeriode.setDisplayedMnemonic('P');
		labelDebut.setDisplayedMnemonic('D');
		labelFin.setDisplayedMnemonic('F');
		labelAdresse.setDisplayedMnemonic('A');
		//labelAdresseFrise.setDisplayedMnemonic('F');
				
		//Couleurs		
		boutonNewFrise.setBackground(Color.LIGHT_GRAY);	
		boutonChoixFrise.setBackground(Color.LIGHT_GRAY);
		saisieAdresse.setBackground(Color.LIGHT_GRAY);
		
		GridBagConstraints contrainte = new GridBagConstraints();		
		contrainte.insets = new Insets(10,10,10,10);
		contrainte.anchor = GridBagConstraints.NORTHWEST;
		
		
		//Date
		contrainte.gridx = 0; contrainte.gridy = 0;		
		this.add(labelCreation,contrainte);
		
		contrainte.gridwidth = 4;
		contrainte.anchor = GridBagConstraints.EAST;
		contrainte.gridx = 1;
		boutonNewFrise.setActionCommand("FRISE");
		this.add(boutonNewFrise,contrainte);
		
		
		contrainte.anchor = GridBagConstraints.NORTHWEST;
		//Titre
		contrainte.gridx = 0;		
		contrainte.gridy = 1;
		this.add(labelTitre,contrainte);
		
		contrainte.gridx = 1;
		contrainte.gridwidth = 3;
		this.add(saisieTitre,contrainte);
		
		//Lieu
		contrainte.gridwidth = 1;
		contrainte.gridx = 0;
		contrainte.gridy = 2;		
		this.add(labelPeriode,contrainte);
		
		contrainte.gridx = 1;
		contrainte.gridwidth = 3;
		this.add(saisiePeriode,contrainte);
		
		//Debut
		contrainte.gridwidth = 1;
		contrainte.gridx = 0;
		contrainte.gridy = 3;		
		this.add(labelDebut,contrainte);
		
		
		contrainte.gridx = 1;
		this.add(saisieDateDebut,contrainte);
			
		
		//Fin
		contrainte.gridx = 0;
		contrainte.gridy = 4;
		this.add(labelFin,contrainte);
		
		contrainte.gridx = 1;
		this.add(saisieDateFin,contrainte);
		
		contrainte.gridx = 0;
		contrainte.gridy = 5;
		this.add(labelAdresse,contrainte);
		
		contrainte.gridx = 1;
		saisieAdresse.setActionCommand("SAUVEGARDE");
		this.add(saisieAdresse,contrainte);
		
		contrainte.gridx = 0; contrainte.gridy = 6;
		this.add(separateur,contrainte);
		
		contrainte.gridx = 0; contrainte.gridy = 7;		
		this.add(labelChoixFrise,contrainte);	
		
		contrainte.gridwidth = 1;
		contrainte.anchor = GridBagConstraints.EAST;
		contrainte.gridx = 1;
		boutonChoixFrise.setActionCommand("SELECTION");
		this.add(boutonChoixFrise,contrainte);
		
		contrainte.gridwidth = 1;
		
		
		contrainte.gridy = 8;
		contrainte.gridx = 0;
		//this.add(labelAdresseFrise, contrainte);
		
		contrainte.gridx = 1;
		//this.add(saisieAdresseFrise,contrainte);
		
		
		reset();
		
	}//PanelFormulaire()	

	
	/**
	 * M�thode qui r�initialise tous les champs de saisies du panel
	 */
	public void reset() {
		//On r�initialise les champs et on met le focus sur la premiere zone
		saisieTitre.setText("");
		saisiePeriode.setText("");
		saisieDateDebut.setText("");
		saisieDateFin.setText("");	
		//saisieAdresse.setText("");
		//saisieAdresseFrise.setText("");
		saisieTitre.requestFocus();		
	}

	
	/**
	 * M�thode qui met le controleur � l'�coute des 2 boutons du panel
	 * pour la synchronisation
	 * @param controleur Controleur
	 */
	public void enregistreEcouteur(Controleur controleur) {
		boutonNewFrise.addActionListener(controleur);
		boutonChoixFrise.addActionListener(controleur);
		saisieAdresse.addActionListener(controleur);
		
	}

	/**
	 * Methode qui renvoie l'intitule du bouton d'ajout
	 * @return String
	 */
	public String getIntituleBoutonAjout() {
		return INTITULE_BOUTON_AJOUT;
	}

	
	/**
	 * Methode qui renvoie une FriseChronologique construite
	 * � partir des donn�es saisies dans les champs
	 * @return FriseChronologique
	 */
	public FriseChronologique getFrise() {
		return new FriseChronologique(saisieTitre.getText(), Integer.parseInt((String) saisiePeriode.getText()),
				Integer.parseInt(saisieDateDebut.getText()),Integer.parseInt(saisieDateFin.getText()),adresseSAV);
	}
	
	/**
	 * Methode qui renvoie le champ qui doit recevoir le focus
	 * ici c'est le 1er aussi donc saisieTitre
	 * @return JTextField
	 */
	public JTextField getChampRecevantFocus() {
		return saisieTitre;
	}
	
	/**
	 * Methode qui modifie l'adresse de sauvegarde de la frise
	 * selon le choix de l'utilisateur
	 * @param parAdresse String
	 */
	public void setAdresseSAV(String parAdresse) {
		adresseSAV = parAdresse;
	}
}
