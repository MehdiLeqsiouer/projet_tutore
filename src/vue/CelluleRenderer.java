package vue;

import java.awt.Component;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import modele.ConstantesCouleursFontes;
import modele.Evenement;

/**
 * Classe qui correspond au renderer de notre JTable
 * Elle h�rite de la classe JLabel et impl�mente l'interface TableCellRenderer
 * @see JLabel
 * @see TableCellRenderer
 * @see JTable
 * @author Mehdi
 *
 */
@SuppressWarnings("serial")
public class CelluleRenderer extends JLabel implements TableCellRenderer{
	
	/**
	 * Constructeur de la classe,
	 * il appel le constructeur m�re et modifie
	 * certains elements des cases comme la couleur...
	 */
	public CelluleRenderer() {
		super();
		setOpaque(false);
		setHorizontalAlignment(JLabel.CENTER);
		this.setForeground(new java.awt.Color(180,100,40));
	}

	
	/** 
	 * Methode qui traite chaque case selon l'objet qui s'y trouve
	 * Si la case est vide, on ne met rien
	 * Sinon on y ajout une Icone, qui correspond � l'image que l'ont va charger dans champs de L'�v�nement
	 * On redimensionne l'image pour qu'elle soit correcte
	 */
	public Component getTableCellRendererComponent(JTable table, Object valeur,
			boolean estSelectionne, boolean aLeFocus, int ligne, int colonne) {
		
		if (valeur != null) {
			Evenement evt = (Evenement) valeur;
			setIcon(new ImageIcon(new ImageIcon(evt.getAdresse()).getImage().getScaledInstance(90, 90, Image.SCALE_DEFAULT)));			
			setText(evt.getTitre());
		}
		else {
			setText("");
			setIcon(null);
		}
		setBackground(ConstantesCouleursFontes.CREAM);
		setToolTipText("Cliquer pour avoir l'�v�nement d�taill�");
		return this;
	}

}
