package modele;
import java.io.Serializable;

 

/**
 * Classe Date qui impl�mente l'interface Comparable et Serializable
 * Une date a 3 champs
 * @see Comparable
 * @see Serializable
 * @author Mehdi
 *
 */
@SuppressWarnings("serial")
public class Date implements Comparable <Date>, Serializable {
  /**
   * Champ jour entier entre 0 et 31	
   */
  private int jour;
  /**
   * Champ mois entier entre 1 et 12
   */
  private int mois;
  /**
   * Champ annee entier
   */
  private int annee;
  
  
  /**
   * Constructeur de la classe Date, affecte les champs
   * @param parJour int
   * @param parMois int 
   * @param parAnnee int
   */
  public Date (int parJour, int parMois, int parAnnee)   {   
	jour = parJour;
	mois = parMois;
	annee = parAnnee; 	
  } 
   
  /**
   * retourne 0 si this et parDate sont egales, 
   * -1 si this precede parDate,
   *  1 si parDate precede this
   */
  public int compareTo (Date parDate) {
    if (annee < parDate.annee)
		return -1;
	if (annee > parDate.annee)
		return 1;
	// les ann�es sont =
	if (mois < parDate.mois)
		return -1;
	if (mois > parDate.mois)
		return 1;
	// les mois sont =
	if (jour < parDate.jour)
		return -1;
	if (jour > parDate.jour)
		return 1;
	return 0;	
  }
   
  /**
   * Methode toString() qui retourne un affichage
   * d'une date
   * @return String chaine � afficher
   */
  public String toString () {
    String chaine = new String();
    /*switch (jourSemaine) {
		 case 1: chaine = "dimanche"; break;
		 case 2: chaine = "lundi"; break;
		 case 3: chaine = "mardi"; break;
		 case 4: chaine = "mercredi"; break;
		 case 5: chaine = "jeudi"; break;
		 case 6: chaine = "vendredi"; break;
		 case 7: chaine = "samedi"; break;
		}*/
	chaine += jour + " ";
	switch (mois) {
		 case 1: chaine += "janvier"; break;
		 case 2: chaine += "fevrier"; break;
		 case 3: chaine += "mars"; break;
		 case 4: chaine += "avril"; break;
		 case 5: chaine += "mai"; break;
		 case 6: chaine += "juin"; break;
		 case 7: chaine += "juillet"; break;
		 case 8: chaine += "aout"; break;
		 case 9: chaine += "septembre"; break;
		 case 10: chaine += "octobre"; break;
		 case 11: chaine += "novembre"; break;
		 case 12: chaine += "decembre"; break;
		}
	chaine += " " + annee;
	return chaine;
  }  

  	
  	/**
  	 * Accesseur du champ Annee
  	 * @return int
  	 */
	public int getAnnee() { 
		return annee;
	}
	
	/**
	 * Accesseur du champ jour
	 * @return int
	 */
	public int getJour() { 
		return jour;
	}
	
	/**
	 * Accesseur du champ jour
	 * @return int
	 */
	public int getMois() { 
		return mois;
	}

}  // class Date