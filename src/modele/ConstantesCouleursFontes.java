package modele;

import java.awt.Color;
import java.awt.Font;

/**
 * Interface ConstantesCouleursFontes
 * On stock ici nos couleurs et nos fonts afin de les 
 * Récuperer facilement
 * @see Color
 * @see Font
 * @author Mehdi
 *
 */
public interface ConstantesCouleursFontes {
	
	final Color MASTIC = new Color (179,177,145);
	final Color VANILLE = new Color (225,206,154);
	final Color ROUGE = new Color (150,15,15);
	final Color VERT = new Color (150,150,15);
	final Color BLEU = new Color (22,159,182);
	final Color CREAM = new Color (253,241,184);
	
	final Font FONT_14 = new Font ("Verdana", Font.BOLD,14);
	final Font FONT_12 = new Font ("Verdana", Font.BOLD,12);
	final Font FONT_11 = new Font ("Verdana", Font.BOLD,11);

}
