package modele;

/**
 * Classe constante Data
 * Qui repertorie toutes nos constantes
 * Utile si l'on veut changer la langue par exemple
 * @author Driss
 *
 */
public final class Data {
	public static String [] nomDesMois = {"Janvier","F�vrier","Mars","Avril","Mai","Juin","Juillet",
			"Aout","Septembre","Octobre","Novembre","D�cembre"};
	
	public static String[] jour = {"01","02","03","04","05","06",
			"07","08","09","10","11","12",
			"13","14","15","16","17","18",
			"19","20","21","22","23",
			"24","25","26","27","28","29","30","31"};
	
	public static String[] mois = {"01","02","03","04","05","06",
			"07","08","09","10","11","12"};
	
	public static String INTITULE_BOUTON_AJOUT_FRISE = "New Frise";
	public static String INTITULE_BOUTON_AJOUT_FORMULAIRE = "+";
	
	public static String INTITULE_BOUTON_GAUCHE = "<=";
	public static String INTITULE_BOUTON_DROITE = "=>";
	
	public static String [] poids = {"1","2","3","4","5"};
	
	public static String[] INTITULE_ITEM = {"Cr�ation","Affichage","Quitter","?"};
	
	
}
