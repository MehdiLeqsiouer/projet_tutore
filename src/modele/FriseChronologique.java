package modele;


import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.TreeSet;


/**
 * Classe FriseChronologique qui impl�mente l'interface Serializable
 * Une frise est caract�ris�e par un intitule, un ensemble d'�v�nements
 * une date de d�but, une date de fin, une p�riode, et une adresse o� la sauvegarder
 * @see Serializable
 * @see Evenement
 * @author Mehdi
 *
 */
@SuppressWarnings("serial")
public class FriseChronologique implements Serializable{
	
	/**
	 * Nom de la frise
	 */
	private String intitule;
	
	/**
	 * Ensemble d'�v�nement, ils sont tri�s dans l'ordre (TreeSet) et sont associ� a une cl� qui est une ann�e
	 */
	private HashMap <Integer, TreeSet <Evenement>> hashMapEvts;
	
	/**
	 * date de d�but de la frise
	 */
	private int dateDebut;
	
	/**
	 * Date de fin de la frise
	 */
	private int dateFin;
	
	/**
	 * P�riodicit� de la frise
	 */
	private int periode;
	
	/**
	 * Adresse ou enregistrer la frise
	 */
	private String adresseFichier;
	
	
	/**
	 * Constructeur de la classe FriseChronologique
	 * affecte chaque champs et instancie l'ensemble d'�v�nement
	 * @param parIntitule String
	 * @param parPeriode int
	 * @param parDateDebut int 
	 * @param parDateFin int 
	 * @param parAdresse String
	 */
	public FriseChronologique(String parIntitule,int parPeriode,int parDateDebut,int parDateFin,String parAdresse) {
		intitule = parIntitule;
		hashMapEvts = new HashMap <Integer, TreeSet <Evenement>>();
		periode = parPeriode;
		dateDebut = parDateDebut;
		dateFin = parDateFin;
		adresseFichier = parAdresse;		
	}
	
	/**
	 * Constructeur vide qui fait office de test !
	 * Instancie une nouvelle Frise que j'ai moi-m�me d�cid�, et qui servira
	 * d'exemple le jour du projet
	 */
	public FriseChronologique() {
		intitule = "Frise de test";
		hashMapEvts = new HashMap <Integer, TreeSet <Evenement>>();
		periode = 5;
		dateDebut = 1950;
		dateFin = 1980;
		adresseFichier = "C:\\Users\\Mehdi\\Desktop\\IUT_travail\\workspace\\ProjetTutor�\\frises";
	}
	
	/**
	 * Methode ajout qui permet d'ajouter un �v�nement a la frise
	 * On v�rifie si l'ensemble contient la cl� qui est l'ann�e de l'�v�nement
	 * Si oui on l'ajoute � la suite
	 * Si non on cr�e un TreeSet qui aura pour Cl� l'ann�e, et on y ajoute l'�v�nement
	 * @param parEvt Evenment
	 */
	public void ajout(Evenement parEvt) { 
		Date parDate = parEvt.getDate();
		int annee = parDate.getAnnee();
		if(hashMapEvts.containsKey(annee)) {
			hashMapEvts.get(annee).add(parEvt);
		}
		
		else {
			TreeSet <Evenement> set = new TreeSet <Evenement>();
			set.add(parEvt);
			hashMapEvts.put(annee, set);
		}
	}
	
	
	/**
	 * Methode toString qui sert juste � afficher le titre pour l'instant
	 * @return String
	 */
	public String toString() {
		/*String chaine = new String();
		chaine += "Nom de la frise : "+intitule;
		chaine += " P�riode : "+periode;
		for (HashMap.Entry<Integer, TreeSet <Evenement>> entry : hashMapEvts.entrySet())
		{
			chaine += "Annee :" +entry.getKey()+" : "+entry.getValue()+"\n";
		}
		return chaine;*/
		return new String(intitule);
	}	
	
	
	/**
	 * Renvoie la Colletion d'�v�nement � l'ann�e recu en param�tre (la cl�)
	 * @param parAnnee Integer
	 * @return Collection
	 */
	public Collection<Evenement> getEvenementsAnnee(Integer parAnnee) {
		return hashMapEvts.get(parAnnee);
	}
	
	/**
	 * Accesseur du champ hashMapEvts
	 * @return HashMap 
	 */
	public HashMap <Integer, TreeSet <Evenement>> getHashMapEvts() {
		return hashMapEvts;
	}
	
	/**
	 * Accesseur du champ Periode
	 * @return int
	 */
	public int getPeriode() {
		return this.periode;
	}
	
	/**
	 * Accesseur du champ date de d�but
	 * @return int
	 */
	public int getDateDebut() {
		return this.dateDebut;
	}
	
	/**
	 * Accesseur du champ date de fin
	 * @return int
	 */
	public int getDateFin() {
		return this.dateFin;
	}
	
	/**
	 * Accesseur du champ intitule
	 * @return String
	 */
	public String getIntitule() {
		return this.intitule;
	}
	
	/**
	 * Accesseur du champ adresse
	 * @return String
	 */
	public String getAdresse() {
		return this.adresseFichier;
	}

}
