package modele;




import org.junit.Test;

import modele.Evenement;
import modele.FriseChronologique;



/**
 * Classe qui correspond aux  tests unitaires avec JUnit
 * @author Mehdi et Driss
 *
 */
public class JeuDeTest {

	private FriseChronologique frise = new FriseChronologique();
	private ModeleTable modele = new ModeleTable(frise);
	
	@Test
	public void testAjout() {
		Evenement evt = new Evenement(new Date(03,05,1999),"Test","test","test",1,frise);
		frise.ajout(evt);
	}
	
	@Test
	public void testToString() {
		System.out.println(frise.toString());
	}
	
	@Test
	public void testGetEvenementsAnnee() {
		Evenement evt = new Evenement(new Date(03,05,1999),"Test","test","test",1,frise);
		frise.ajout(evt);
		frise.getEvenementsAnnee(evt.getDate().getAnnee());
	}
	
	@Test
	public void testAjoutModele() {
		Evenement evt = new Evenement(new Date(03,05,1960),"Test","test","test",1,frise);
		modele.ajoutEvenement(evt);
	}
}
