package modele;

import java.util.Collection;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 * Modele de notre JTable qui impl�mente l'interface DefaultTableModel
 * On d�finit le nombre de colonnes et de ligne selon
 * les dates de d�but et de fin de notre Frise
 * et on limite les lignes a 5 ( poids entre 1 et 5)
 * Puis on lui ajoute tous les �v�nements en appelant la m�thode
 * ajoutEvenements
 * @see JTable
 * @see DefaultTableModel
 * @author Mehdi
 *
 */
@SuppressWarnings("serial")
public class ModeleTable extends DefaultTableModel {
	/**
	 * Champ debutFrise, on le conserve ici pour pouvoir y acc�der
	 * dans la m�thode  ajoutEvenement
	 */
	private int debutFrise;
	
	/**
	 * Constructeur de ModeleTable qui recoit une frise
	 * Le nombre de colonnes correspond � la plage dates (fin - d�but)
	 * le nombre de lignes correspond au poids possible (5 par d�faut)
	 * Les indentifiants des colonnes sont les ann�es, et ils sont ajout�s
	 * selon la p�riode (tous les 2 ans, tous les 5 ans etc...)
	 * On r�cup�re tous les �v�nements de la Frise et on les ajoute
	 * @param parFrise FriseChronologique
	 */
	public ModeleTable(FriseChronologique parFrise) {		
		
		this.setColumnCount((parFrise.getDateFin()-parFrise.getDateDebut())+1);
		this.setRowCount(5);
		debutFrise = parFrise.getDateDebut();
		String [] colonnes = new String[this.getColumnCount()];
		
		Integer annee = parFrise.getDateDebut();
		for(int i = 0; i < colonnes.length; i ++) {
			if (i%parFrise.getPeriode() == 0) {
				colonnes[i] = annee.toString();
			}
			else
				colonnes[i] = "";
			annee += 1;
		}
		this.setColumnIdentifiers(colonnes);
		
		//On r�cup�re TOUS les �v�nements de la frise et on les a ajoute � la table
		annee = parFrise.getDateDebut();
		for(int i = 0; i < this.getColumnCount(); i ++) {
			Collection <Evenement> evts = parFrise.getEvenementsAnnee(annee);
			if(evts != null) {
				for(Evenement events : evts) {
					ajoutEvenement(events);
				}
			}
			annee += 1;
		}	 
		
	}
	
	/**
	 * Methode ajoutEvenement qui permet d'ajouter un evenement a la table
	 * en calculant sa colonne et sa ligne associ�es
	 * @param parEvt Evenement
	 */
	public void ajoutEvenement(Evenement parEvt) {
		int indiceColonne = parEvt.getDate().getAnnee()-debutFrise;		
		int indiceLigne = parEvt.getPoids()-1;
		setValueAt(parEvt, indiceLigne,indiceColonne);
	}
	
	/**
	 * Methode qui indique si la case aux indices donn�s est modifiable ou non,
	 * ici non dans tous les cas
	 * @param indiceLigne int
	 * @param indiceColonne int
	 * @return boolean
	 */
	public boolean isCellEditable(int indiceLigne, int indiceColonne) {
		return false;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	/**
	 * Methode qui renvoie la classe de l'objet a la case envoy� en param�tre
	 * Ici, ce ne sont que des Evenement
	 * @see Evenement
	 */
	public Class getColumnClass(int parNum) {
		return Evenement.class;
	}

}
