package modele;

import java.io.Serializable;
import java.util.StringTokenizer;



/**
 * Classe Evenement qui impl�mente l'interface Comparable et Serializable
 * Un evenement est caract�ris� par une Date, un titre, une image (adresse � charger),
 * Un texte descriptif, un poids (importance) et une Frise associ�e
 * @see Comparable
 * @see Serializable
 * @author Driss
 *
 */
@SuppressWarnings("serial")
public class Evenement implements Comparable<Evenement>, Serializable{
	/**
	 * Date de l'�v�nement (jour mois annee)
	 * @see Date
	 */
	private Date date;
	
	/**
	 * Champ titre String de l'�v�nement
	 */
	private String titre;
	
	/**
	 * Adresse de l'image String (chemin absolu)
	 */
	private String adresseFichier;
	
	/**
	 * Champ texte String qui d�crit l'�v�nement
	 */
	private String texteDescriptif;
	
	/**
	 * Poids int : importance de l'�v�nement entre 1 et 5
	 */
	private int poids;
	
	/**
	 * Frise � laquelle on ajoute l'ev�nement
	 * @see FriseChronologique
	 */
	private FriseChronologique frise;

	
	/**
	 * Constructeur de la classe Evenement
	 * Affecte chaque champs...
	 * @param parDate Date
	 * @param parTitre String
	 * @param parAdresse String
	 * @param parTexte String
	 * @param parPoids int
	 * @param parFrise FriseChronologique
	 */
	public Evenement(Date parDate,String parTitre, String parAdresse,String parTexte,int parPoids,FriseChronologique parFrise) {
		date = parDate;
		titre = parTitre;
		adresseFichier = parAdresse;
		texteDescriptif = parTexte;
		poids = parPoids;
		frise = parFrise;		
	}

	
	/**
	 * Methode toString qui renvoie une chaine � afficher
	 * On utilise StringTokenizer pour revenir � la ligne tous
	 * les 12 mots
	 * La chaine est encadr�e par du HTML
	 * @see StringTokenizer
	 * @return String
	 */
	public String toString() {
		String chaine = new String("<html>");
		chaine += "<h4><i>"+date + "</i></h4><br/>"
				+ "<h3>"+titre+"</h3> <br/><br/>";
			
		chaine += "<p>";
		StringTokenizer st = new StringTokenizer(texteDescriptif);
		while (st.hasMoreTokens()) {
			if (st.countTokens() %12 == 0 && st.countTokens() != 0)
				chaine += "<br/>";
			chaine += " "+st.nextToken();		
		}
		chaine += "</p></html>";
		return chaine;
	}

	/**
	 * Accesseur du champ Date
	 * @return Date
	 */
	public Date getDate(){
		return this.date;
	}

	/**
	 * Accesseur du champ Titre
	 * @return String
	 */
	public String getTitre(){
		return this.titre;
	}

	/**
	 * Accesseur du champ Texte
	 * @return String
	 */
	public String getTexte(){
		return this.texteDescriptif;
	}

	/**
	 * Accesseur du champ Poids
	 * @return int
	 */
	public int getPoids(){
		return this.poids;
	}
	
	/**
	 * Accesseur du champ Frise
	 * @return FriseChronologique
	 */
	public FriseChronologique getFrise() {
		return this.frise;
	}
	
	/**
	 * Accesseur du champ Adresse
	 * @return String
	 */
	public String getAdresse() {
		return this.adresseFichier;
	}

	/**
	 * Methode compareTo � surcharger
	 * Renvoie -1 si 2 evenements sont totalement diff�rent
	 * renvoie 1 si ils ont une date ou un titre en commun
	 * renvoie 0 si ils sont les m�mes
	 * @return int
	 */
	public int compareTo(Evenement parEvt){
		if (this.date.compareTo(parEvt.date) == 0){
			if (this.titre.compareTo(parEvt.titre) == 0){
				if (this.texteDescriptif.compareTo(parEvt.texteDescriptif) == 0)
					return 0;
				return 1;
			}
			return 1;
		}
		return -1;
	}
}//Classe Evenement




